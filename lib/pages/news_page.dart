import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Inshorts/models/news_data.dart';
import 'package:Inshorts/screens/news_read_screen.dart';
import 'package:Inshorts/screens/image_show_screen.dart';
import 'package:Inshorts/models/news_info.dart';
import 'package:Inshorts/services/api_manager.dart';
import 'package:intl/intl.dart';

class NewsPage extends StatefulWidget {
  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  Future<NewsModel> _newsModel;

  @override
  void initState() {
    _newsModel = API_Manager().getNews();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // final _size = MediaQuery.of(context).size;
    return Container(
      height: MediaQuery.of(context).size.height * 0.85,
      child: FutureBuilder<NewsModel>(
        future: _newsModel,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return PageView.builder(
              scrollDirection: Axis.vertical,
              pageSnapping: true,
              itemCount: snapshot.data.articles.length,
              itemBuilder: (context, index) {
                // print(snapshot.data.articles.length);
                // print(snapshot.data.totalResults);
                // print(snapshot.data.articles[0].description);
                // print('');
                var article = snapshot.data.articles[index];
                var formattedTime =
                    DateFormat('dd MMM - HH:mm').format(article.publishedAt);
                return PageView(
                  children: [
                    Container(
                      color: Colors.white,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ImageShow(
                                            imageUrl: article.urlToImage,
                                            headline: article.title,
                                          )));
                            },
                            child: Hero(
                              tag: article.urlToImage,
                              child: Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.4,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: NetworkImage(article.urlToImage)),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              article.title,
                              style: TextStyle(
                                  fontSize: 21, fontWeight: FontWeight.w600),
                              maxLines: 2,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                              height: MediaQuery.of(context).size.height * 0.4,
                              child: Text(
                                article.content,
                                maxLines: 10,
                                style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                CupertinoPageRoute(
                                  builder: (context) =>
                                      NewsReadScreen(url: article.url),
                                ),
                              );
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "Swipe right to read full article",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    NewsReadScreen(url: article.url),
                  ],
                );
              },
            );
          } else
            return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
