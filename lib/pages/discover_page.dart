import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Inshorts/screens/settings_screen.dart';

class DiscoverPage extends StatefulWidget {
  @override
  _DiscoverPageState createState() => _DiscoverPageState();
}

class _DiscoverPageState extends State<DiscoverPage> {
  List<String> top = ["assets/images/poll.png", "assets/images/insights.png"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        // height: MediaQuery.of(context).size.height,
        child: ListView(
          children: [
            Container(
              // height: 100,
              child: Column(
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                    child: Container(
                      height: 40,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  CupertinoPageRoute(
                                      builder: (context) => SettingsScreen()));
                            },
                            child: Icon(
                              Icons.settings,
                              color: Colors.blueAccent,
                            ),
                          ),
                          Text(
                            "Discover",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                          Container()
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 8.0, right: 8, top: 10),
                    child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                        color: Colors.grey.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: TextField(
                        cursorColor: Colors.black.withOpacity(0.5),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          prefixIcon: Icon(Icons.search, color: Colors.black),
                          hintText: 'Search',
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Divider(),
            Container(
              height: 680,
              child: ListView(
                children: [
                  Container(
                    height: 140,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: 2,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.only(
                              top: 20.0, left: 10, right: 10),
                          child: Container(
                            height: 120,
                            width: 200,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                image: DecorationImage(
                                    image: AssetImage(top[index]),
                                    fit: BoxFit.cover)),
                          ),
                        );
                      },
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: 10, top: 20),
                      child: Text(
                        'Categories',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w800,
                            color: Colors.black),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: 10, top: 5),
                      child: Container(
                        width: 30,
                        height: 3,
                        decoration: BoxDecoration(
                            color: Colors.blueGrey,
                            borderRadius: BorderRadius.circular(10)),
                      ),
                    ),
                  ),
                  Container(
                    height: 120,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: 5,
                        itemBuilder: (context, index) {
                          return Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, left: 10, right: 10),
                              child: Column(
                                children: [
                                  IconButton(
                                    onPressed: () {},
                                    icon: categories[index].icon,
                                    iconSize: 35,
                                    color: Colors.blueAccent,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(8),
                                    child: Center(
                                      child: Text(
                                        categories[index].category,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            color: Colors.black),
                                      ),
                                    ),
                                  )
                                ],
                              ));
                        }),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: 10, top: 20),
                      child: Text(
                        'Suggested Topics',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w800,
                            color: Colors.black),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: 10, top: 5),
                      child: Container(
                        width: 30,
                        height: 3,
                        decoration: BoxDecoration(
                            color: Colors.blueGrey,
                            borderRadius: BorderRadius.circular(10)),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    height: 550,
                    child: GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3),
                      itemBuilder: (context, index) {
                        return SizedBox(
                          height: 200,
                          child: Card(
                            shadowColor: Colors.blue,
                            elevation: 2,
                            child: Stack(
                              children: [
                                Container(
                                  height: 180,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Image.asset(
                                    suggestedTopics[index].image,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Positioned(
                                  child: Text(
                                    suggestedTopics[index].topic,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w800),
                                  ),
                                  bottom: 2,
                                  left: 2,
                                )
                              ],
                            ),
                          ),
                        );
                      },
                      itemCount: suggestedTopics.length,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Categories {
  final Icon icon;
  final String category;

  Categories({this.icon, this.category});
}

List<Categories> categories = [
  Categories(icon: Icon(Icons.mobile_screen_share), category: "My Feed"),
  Categories(icon: Icon(Icons.new_releases), category: "All News"),
  Categories(icon: Icon(Icons.star), category: "Top Stories"),
  Categories(icon: Icon(Icons.terrain), category: "Trending"),
  Categories(icon: Icon(Icons.bookmark), category: "Bookmarks"),
  Categories(icon: Icon(Icons.remove_red_eye), category: "Unread"),
];

class SuggestedTopics {
  final String image;
  final String topic;

  SuggestedTopics({this.image, this.topic});
}

List<SuggestedTopics> suggestedTopics = [
  SuggestedTopics(image: "assets/images/corona.png", topic: "Cornavirus"),
  SuggestedTopics(image: "assets/images/india.png", topic: "India"),
  SuggestedTopics(
      image: "assets/images/international.png", topic: "International"),
  SuggestedTopics(image: "assets/images/business.png", topic: "Business"),
  SuggestedTopics(image: "assets/images/politics.png", topic: "Politics"),
  SuggestedTopics(image: "assets/images/sports.png", topic: "Sports"),
  SuggestedTopics(image: "assets/images/technology.png", topic: "Technology"),
  SuggestedTopics(image: "assets/images/startups.png", topic: "Startups"),
  SuggestedTopics(
      image: "assets/images/entertainment.png", topic: "Entertainment"),
  SuggestedTopics(image: "assets/images/health.png", topic: "Health"),
  SuggestedTopics(image: "assets/images/automobile.png", topic: "Automobile"),
  SuggestedTopics(image: "assets/images/fashion.png", topic: "Fashion"),
];
