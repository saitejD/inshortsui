import 'package:flutter/material.dart';

class NewsData {
  final String image;
  final String headline;
  final String detailedNews;

  NewsData({@required this.image, this.headline, this.detailedNews});
}

List<NewsData> newsData = [
  NewsData(
      image: "assets/images/modi.jpg",
      headline:
          "PM Narendra Modi hints at election dates for four states by March 7",
      detailedNews:
          """This year, it is my assumption that the EC will announce the dates by March 7 he said on his third visit in a month to poll-bound Assam.
             PM Modi later travelled to Bengal’s Hooghly, where he spoke of ashol parivartan (real change). Sources said the poll schedule for the state might be announced after Modi holds a rally at Kolkata Brigade Parade Ground early next month. The rally will be the culmination of six parivartan yatras by BJP across the poll-bound state.
             In Assams Silapathar, the PM dedicated three energy infrastructure projects in the state worth over Rs 3,000 crore to the people. He also inaugurated the state s seventh engineering college at Dhemaji and laid the foundation for the construction of the eighth such college at Sualkuchi near Guwahati."""),
  NewsData(
      image: "assets/images/jeffbezzes.jpg",
      headline:
          """India's Enforcement Directorate to examine findings in report on Amazon: Source""",
      detailedNews:
          """The Reuters story was based on internal Amazon documents dated between 2012 and 2019. It provided an inside look at the cat-and-mouse game Amazon has played with India's government, adjusting its corporate structures each time the government imposed new restrictions aimed at protecting small traders.
          \n19 Feb, 2021, 08.45 AM IST"""),
  NewsData(
      image: "assets/images/elonemusk.jpeg",
      headline:
          """Elon Musk's Video From 2008 Explaining Why Tesla Cars are So Expensive Goes Viral""",
      detailedNews:
          """But the real highlight of the video is his vision, back almost 13 years ago about EVS, "What's the point of expensive cars for rich people? The point is, when you have new technology, it takes time to optimize that technology." He then uses a more common example to explain it, "If you think back to the early days of laptops or cell phones or anything new, it's expensive in the early days. The first job you have with new technology is that you have to make it work. The critical point is you can't get to the low-cost cars unless you start with the expensive ones."""),
  NewsData(
      image: "assets/images/trump.jpg",
      headline:
          """Trump slams Supreme Court for not shielding his tax records""",
      detailedNews:
          """Former US President Donald Trump issued a statement in which he slammed the US Supreme Court for not blocking New York prosecutors from obtaining his tax records.\n\nEarlier on Monday, US Supreme Court Justice Stephen Breyer declined to issue a stay for a lower court ruling in favour of former President Donald Trump disclosing his tax returns to a New York grand jury.
          "The Supreme Court never should have let this 'fishing expedition' happen, but they did," Trump said on Monday. "This is something which has never happened to a President before, it is all Democrat-inspired in a totally Democrat location, New York City and State, completely controlled and dominated by a heavily reported enemy of mine, Governor Andrew Cuomo."
          Trump further said the New York Attorney General's investigation is a continuation of the greatest political hunt in US history, adding that the probe is similar to practices in Third World countries and represents a threat to the foundation of liberty in the United States. Trump also reiterated his stance that the 2020 US presidential election was rigged and stolen from him."""),
  NewsData(
      image: "assets/images/mumbainews.jpg",
      headline:
          """Mumbai News Live Updates: CM Thackeray to hold meeting with BMC to review COVID-19 situation""",
      detailedNews:
          """The number of recoveries crossed the 3-lakh mark in Mumbai city amid concerns over rising Coronavirus cases. The city reported 760 new COVID-19 cases on Monday, a drop from 921 a day earlier, and four fresh fatalities. However, the number of active patients under treatment increased to 7,397. Home Minister Anil Deshmukh has warned that the cyber cell is keeping a close eye on those who are spreading false rumors that lockdown has been reimposed in Maharashtra and warned of stringent action. Meanwhile, Transport Minister Anil Parab said that the minimum fare for Mumbai's autorickshaws and taxis will be Rs 21 and Rs 25 respectively from March 1 after a hike of Rs 3 was approved.
              Thane: An elevator system in an 18-storey building in the Khopat area used as rental accommodation for project-affected people developed a snag following which the lift capsule stationed at the ground floor suddenly shot upwards and got locked near the top floor of the structure in the wee hours of Tuesday, officials said. Residents said they were awakened by a huge sound around 1.40 am and rushed out and noticed the malfunction. No one was injured as the lift was empty. The initial probe revealed the pulley chain to have malfunctioned.
              facebook.
              The number of recoveries crossed the 3-lakh mark in Mumbai city amid concerns over rising Coronavirus cases. The city reported 760 new COVID-19 cases on Monday, a drop from 921 a day earlier, and four fresh fatalities. However, the number of active patients under treatment increased to 7,397. Home Minister Anil Deshmukh has warned that the cyber cell is keeping a close eye on those who are spreading false rumors that lockdown has been reimposed in Maharashtra and warned of stringent action. Meanwhile, Transport Minister Anil Parab said that the minimum fare for Mumbai's autorickshaws and taxis will be Rs 21 and Rs 25 respectively from March 1 after a hike of Rs 3 was approved.
              Thane: An elevator system in an 18-storey building in the Khopat area used as rental accommodation for project-affected people developed a snag following which the lift capsule stationed at the ground floor suddenly shot upwards and got locked near the top floor of the structure in the wee hours of Tuesday, officials said. Residents said they were awakened by a huge sound around 1.40 am and rushed out and noticed the malfunction. No one was injured as the lift was empty. The initial probe revealed the pulley chain to have malfunctioned.
              facebook.
              The number of recoveries crossed the 3-lakh mark in Mumbai city amid concerns over rising Coronavirus cases. The city reported 760 new COVID-19 cases on Monday, a drop from 921 a day earlier, and four fresh fatalities. However, the number of active patients under treatment increased to 7,397. Home Minister Anil Deshmukh has warned that the cyber cell is keeping a close eye on those who are spreading false rumors that lockdown has been reimposed in Maharashtra and warned of stringent action. Meanwhile, Transport Minister Anil Parab said that the minimum fare for Mumbai's autorickshaws and taxis will be Rs 21 and Rs 25 respectively from March 1 after a hike of Rs 3 was approved.
              Thane: An elevator system in an 18-storey building in the Khopat area used as rental accommodation for project-affected people developed a snag following which the lift capsule stationed at the ground floor suddenly shot upwards and got locked near the top floor of the structure in the wee hours of Tuesday, officials said. Residents said they were awakened by a huge sound around 1.40 am and rushed out and noticed the malfunction. No one was injured as the lift was empty. The initial probe revealed the pulley chain to have malfunctioned.
              facebook.
              Facebook will pay some Australian publishers as it reaches a deal with govt, will restore news links
              After blocking news content in Australia for a few days, Facebook has cracked a deal with the government. However, the restoring of news on Facebook in Australia comes with a warning. The latest development comes days after Australian Prime Minister Scott Morrison said that the social media giant was back at the negotiating table.
              In a statement released by Facebook (via Alex Kantrowitz), Campbell Brown, Head of News Partnerships, said, "We're restoring news on Facebook in Australia in the coming days."
              It's worth noting that the changes come only after Treasurer Josh Frydenberg and Communications Minister Paul Fletcher accepted the amendments. The government has also cleared the way for the legislation’s passage as early as Wednesday.
              At the moment, it is not clear how much Facebook would be paying media organisations as the statement only says it will "support." But it is more than likely that Facebook has agreed to the Australian government's proposed law that would require Facebook and other major platforms like Google to pay the news publishers for content.
              "After further discussions with the Australian government, we have come to an agreement that will allow us to support the publishers we choose to, including small and local publishers. We're restoring news on Facebook in Australia in the coming days," read the statement.
              It seems that the amendments will offer clarity to digital platforms and news media businesses around the code. Australian government stressed that these codes are intended to strengthen the framework for ensuring news media businesses are fairly remunerated."""),
];
