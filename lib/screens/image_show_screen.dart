import 'package:Inshorts/models/news_info.dart';
import 'package:flutter/material.dart';

class ImageShow extends StatefulWidget {
  final String imageUrl;
  final String headline;

  ImageShow({this.imageUrl, this.headline});

  @override
  _ImageShowState createState() => _ImageShowState();
}

class _ImageShowState extends State<ImageShow> {
  // final TransformationController _controller = TransformationController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            Hero(
              tag: widget.imageUrl,
              child: InteractiveViewer(
                // transformationController: _controller,
                // onInteractionEnd: (details) {
                //   _controller.value = Matrix4.identity();
                // },
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.5,
                      child: Image.network(
                        widget.imageUrl,
                        // fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 10,
              left: 20,
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.9,
                child: Text(
                  widget.headline,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
