import 'dart:async';
import 'package:flutter/material.dart';
// import 'package:Inshorts/models/news_data.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NewsReadScreen extends StatefulWidget {
  final String url;

  const NewsReadScreen({@required this.url});
  @override
  _NewsReadScreenState createState() => _NewsReadScreenState();
}

class _NewsReadScreenState extends State<NewsReadScreen> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.arrow_back_ios),
        elevation: 0,
        title: Text(widget.url),
        actions: [
          GestureDetector(
            onTap: () {},
            child: Icon(Icons.more_vert),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: WebView(
            initialUrl: widget.url,
            onWebViewCreated: (WebViewController webViewController) {
              _controller.complete(webViewController);
            },
          ),
        ),
      ),
    );
  }
}
