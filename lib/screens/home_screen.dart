import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Inshorts/pages/news_page.dart';
import 'package:Inshorts/pages/discover_page.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool isVisible = false;
  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    return DefaultTabController(
      length: 2,
      initialIndex: 1,
      child: Scaffold(
          backgroundColor: Colors.white,
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: double.infinity,
            child: Column(
              children: [
                Visibility(
                  visible: isVisible,
                  child: TabBar(
                    tabs: [
                      Tab(
                        text: "Discover",
                      ),
                      Tab(
                        text: "My Feed",
                      )
                    ],
                  ),
                ),
                Container(
                  height: _size.height,
                  child: TabBarView(
                    children: [
                      DiscoverPage(),
                      NewsPage(),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
