import 'package:flutter/material.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  String dropValue = 'english';
  String dropValue2 = 'off';
  bool switch1 = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.black,
          title: Text("Settings"),
        ),
        body: ListView(
          children: [
            Container(
              height: 200,
              color: Colors.blue,
              child: Padding(
                padding: const EdgeInsets.only(
                  top: 20.0,
                  left: 15,
                  right: 10,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Save Your Preferences",
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text(
                        "Sign in to save your Likes",
                        style: TextStyle(
                          color: Colors.black.withOpacity(0.8),
                        ),
                      ),
                    ),
                    Text(
                      "and your bookmarks",
                      style: TextStyle(
                        color: Colors.black.withOpacity(0.8),
                      ),
                    ),
                    SizedBox(height: 40),
                    Row(
                      children: [
                        Container(
                          height: 50,
                          width: 90,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.black,
                          ),
                          child: Card(
                            color: Colors.black,
                            elevation: 2,
                            child: Center(
                              child: Text(
                                "Sing In",
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.blueAccent,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 100),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: CircleAvatar(
                            backgroundColor: Colors.black,
                            child: Icon(
                              Icons.call,
                              color: Colors.blueAccent,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: CircleAvatar(
                            backgroundColor: Colors.black,
                            child: Text(
                              "f",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: CircleAvatar(
                            backgroundColor: Colors.black,
                            child: Text(
                              "G",
                              style: TextStyle(
                                  color: Colors.lightGreen,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 50.0, top: 15, bottom: 10),
              child: Container(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Auto Start",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.black.withOpacity(0.6),
                    ),
                  ),
                  SizedBox(height: 5),
                  Text(
                    "Please enable to reciver notifications",
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.6),
                    ),
                  ),
                ],
              )),
            ),
            Divider(
              color: Colors.grey,
            ),
            SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 10),
              child: Row(
                children: [
                  Text(
                    "Aa",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(width: 20),
                  Text(
                    "Language",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.black.withOpacity(0.6),
                    ),
                  ),
                  SizedBox(width: 150),
                  DropdownButtonHideUnderline(
                    child: DropdownButton(
                      value: dropValue,
                      onChanged: (value) {
                        setState(() {
                          dropValue = value;
                        });
                      },
                      items: [
                        DropdownMenuItem(
                          value: "english",
                          child: Text(
                            "English",
                            style: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                        DropdownMenuItem(
                          value: "telugu",
                          child: Text(
                            "Telugu",
                            style: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.w600),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 60.0, top: 5),
              child: Container(
                height: 0.5,
                color: Colors.grey,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 10),
              child: Row(
                children: [
                  Icon(Icons.notifications),
                  SizedBox(width: 20),
                  Text(
                    "Notifications",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.black.withOpacity(0.6),
                    ),
                  ),
                  SizedBox(width: 140),
                  Switch(
                    onChanged: (value) {
                      setState(() {
                        switch1 = !switch1;
                      });
                    },
                    value: !switch1,
                    activeColor: Colors.blue,
                    inactiveTrackColor: Colors.black,
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 60.0, top: 5),
              child: Container(
                height: 0.5,
                color: Colors.grey,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 10),
              child: Row(
                children: [
                  Icon(
                    Icons.image,
                    color: Colors.black,
                  ),
                  SizedBox(width: 20),
                  Text(
                    "HD Images",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.black.withOpacity(0.6),
                    ),
                  ),
                  SizedBox(width: 150),
                  Switch(
                    onChanged: (value) {
                      setState(() {
                        switch1 = !switch1;
                      });
                    },
                    value: switch1,
                    activeColor: Colors.blue,
                    inactiveTrackColor: Colors.black,
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 60.0, top: 5),
              child: Container(
                height: 0.5,
                color: Colors.grey,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 10),
              child: Row(
                children: [
                  Icon(Icons.visibility_off),
                  SizedBox(width: 20),
                  Text(
                    "Night Mode",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.black.withOpacity(0.6),
                    ),
                  ),
                  SizedBox(width: 140),
                  Switch(
                    onChanged: (value) {
                      setState(() {
                        switch1 = !switch1;
                      });
                    },
                    value: !switch1,
                    activeColor: Colors.blue,
                    inactiveTrackColor: Colors.black,
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 60.0, top: 5),
              child: Container(
                height: 0.5,
                color: Colors.grey,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 10),
              child: Row(
                children: [
                  Icon(Icons.play_arrow),
                  SizedBox(width: 20),
                  Text(
                    "Auto Play",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.black.withOpacity(0.6),
                    ),
                  ),
                  SizedBox(width: 160),
                  DropdownButtonHideUnderline(
                    child: DropdownButton(
                      value: dropValue2,
                      onChanged: (value) {
                        setState(() {
                          dropValue2 = value;
                        });
                      },
                      items: [
                        DropdownMenuItem(
                          value: "on",
                          child: Text(
                            "On",
                            style: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                        DropdownMenuItem(
                          value: "off",
                          child: Text(
                            "Off",
                            style: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.w600),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0, top: 40),
              child: Text(
                "Share this app",
                style: TextStyle(color: Colors.black),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0, top: 40),
              child: Text(
                "Rate this app",
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0, top: 40),
              child: Text(
                "Feedback",
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0, top: 40),
              child: Text(
                "Terms & conditions",
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0, top: 40),
              child: Text(
                "Privacy",
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            SizedBox(height: 30),
          ],
        ));
  }
}
